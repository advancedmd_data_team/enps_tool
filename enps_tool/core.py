#!/usr/bin/env python3

import re
import pathlib
import datetime

import numpy as np
import pandas as pd
import great_expectations as ge

EXPECTATIONS_PATH = pathlib.Path(__file__).parent.joinpath('output_expectations.json')


class Survey:
    """Class for keeping track of an enps/engagement survey."""

    emp_engagement_sections_dict = {
        'know expectations': 'Get',
        'have needed equipment': 'Get',
        'have opportunity to do best': 'Give',
        'seven days received praise': 'Give',
        '7 days received praise': 'Give',
        'cares about me as a person': 'Give',
        'encourages my development': 'Give',
        'opinions count': 'Belong',
        'mission makes job important': 'Belong',
        'coworkers doing quality work': 'Belong',
        'have best friend at work': 'Belong',
        'last 6mo talked about my progress': 'Grow',
        'last year had opportunities to learn': 'Grow',
        'see myself working here next year': 'Extra',
        'reasonable amount of work': 'Extra',
    }
    likert_columns_proper = [
        'know_expectations',
        'have_needed_equipment',
        'have_opportunity_to_do_best',
        '7_days_received_praise',
        'cares_about_me_as_a_person',
        'encourages_my_development',
        'opinions_count',
        'mission_makes_job_important',
        'coworkers_doing_quality_work',
        'have_best_friend_at_work',
        'last_6mo_talked_about_my_progress',
        'last_year_had_opportunities_to_learn',
        'see myself working here next year',
        'reasonable amount of work',
    ]
    final_columns_order = [
        'id', 'role', 'dept', 'nps_score', 'nps_text_answer', 'anything_else_share', 'NPS_group', 'NPS_group_desc',
        'likert_question', 'answer', 'answer_int', 'engagement_score', 'gallup_eng_level', 'engagement_level',
        'engaged', 'not_engaged', 'actively_disengaged', 'date_added',
        'company', 'team', 'name_optional', 'survey_date', 'supervisor', 'location',
    ]

    def __init__(self, data_file: str, likert_columns: list, other_columns: dict, survey_date: str, company: str,
                 save_to_sql_tbl: bool = False, remove_incomplete: bool = False, complete_indicator_col: str = None, **kwargs):

        self.data= self.step_1_read_in_file(data_file)
        self.company = company
        self.survey_date = survey_date
        self.other_columns = other_columns
        self.likert_columns = likert_columns
        self.save_to_sql_tbl = save_to_sql_tbl
        self.remove_incomplete = remove_incomplete
        self.complete_indicator_col = complete_indicator_col
        # Set any extra kwargs that we don't have explicitly set out
        self.__dict__.update(kwargs)

    @staticmethod
    def _encode_likert_text_to_int(x) -> int:
        """Use the number in the question scale."""
        fallback = 6

        if isinstance(x, str):
            num_found = re.search(r'\d+', x)
            if num_found:
                return int(num_found.group())
            else:
                return fallback

        elif pd.isna(x):
            return fallback

        else:
            print(f"Warning, seeing unexpected value: {x}")
            return fallback

    @staticmethod
    def _encode_likert_num_to_text(x) -> str:
        """Use the number in the question scale. If NA throw a 99"""
        if not isinstance(x, str):
            x = str(x)

        if x == '1':
            return 'Strongly Disagree'
        elif x == '2':
            return 'Disagree'
        elif x == '3':
            return 'Neither Agree nor Disagree'
        elif x == '4':
            return 'Agree'
        elif x == '5':
            return 'Strongly Agree'
        else:
            return 'NA'

    @staticmethod
    def _engagement_scores_gallup(x) -> str:
        if x > 0:
            if x <= 2:
                return 'Actively disengaged'
            elif x <= 4:
                return 'Not engaged'
            elif x == 5:
                return 'Engaged'
            else:
                return 'NA'
        else:
            return 'NA'

    @staticmethod
    def step_1_read_in_file(data_file):
        file_path = pathlib.Path(data_file)
        if file_path.suffix.startswith('.xls'):
            df = pd.read_excel(data_file)
        elif file_path.suffix == '.csv':
            df = pd.read_csv(data_file, encoding='latin-1')
        else:
            raise ValueError(f"Not sure how to handle file with extension: {file_path.suffix}")
        return df

    def step_2a_remove_incomplete_entries(self):
        """Check if we need to remove any incomplete records"""
        # df = step_2_df.copy()
        df = self.data
        # breakpoint()
        if self.remove_incomplete:
            if self.complete_indicator_col in df.columns.tolist():
                print(f"Attempting to remove incomplete values. Using col {self.complete_indicator_col}, val = 'complete'")
                df = df.query(f"`{self.complete_indicator_col}` == 'completed'")
            else:
                # Look for missing entries in key columns
                df = df.dropna(
                    subset=self.likert_columns + [self.other_columns.get('nps_score')],
                    how='any'
                )

        return df

    def step_2_establish_columns(self, dat) -> tuple:

        df = dat.copy()

        # Ensure that our likert_columns are really all there
        if not set(self.likert_columns).issubset(set(df.columns)):
            print("The likert cols as specified don't match the DF!")
            print(f"These columns were not found: {[i for i in self.likert_columns if i not in df.columns]}")
            raise ValueError

        likert_columns_rename = dict(zip(self.likert_columns,
                                         [i.replace('_', ' ')
                                          for i in self.likert_columns_proper]))

        # Rename other columns, first we need to flip the dict around
        other_columns_rename = {v: k for k, v in self.other_columns.items()}

        return (df
                .rename(columns=likert_columns_rename)
                .rename(columns=other_columns_rename)
                ), likert_columns_rename

    def step_3_melt(self, step_2_df, step_2_rename_dict):
        id_variables = [k for k, v in self.other_columns.items() if v]
        return pd.melt(step_2_df.copy(),
                       id_vars=id_variables,
                       value_vars=list(step_2_rename_dict.values()),
                       var_name='likert_question',
                       ).sort_values(['id', 'likert_question'])

    def step_4_map_answer_columns(self, step_3_df):
        # Determine what to do with the value
        df = step_3_df.copy()

        # print(df['value'].value_counts(dropna=False))

        if pd.api.types.is_numeric_dtype(df['value']):
            df = df.assign(answer_int=lambda d: d['value'].astype(int))

        elif df['value'].str.isnumeric().all():
            # print("The values are numeric, renaming to 'answer_int' and mapping for strings")
            df = df.assign(answer_int=lambda d: d['value'].astype(int))

        else:
            # print("The values are strings, renaming to 'answer' and mapping for ints")
            df = df.assign(answer_int=lambda d: d['value'].apply(self._encode_likert_text_to_int))

        return (df
                .assign(answer=lambda d: d['answer_int'].apply(self._encode_likert_num_to_text))
                .drop(columns=['value'], errors='ignore'))

    def step_5_other_col_calc(self, step_4_df):

        return (step_4_df.copy()
                .assign(NPS_group=lambda d: np.where(d['nps_score'] < 7, -1,
                                                     np.where(d['nps_score'] >= 9, 1, 0)))
                .assign(NPS_group_desc=lambda d: d['NPS_group'].map({1: 'Promoter', -1: 'Detractor', 0: 'Neutral'}),
                        gallup_eng_level=lambda d: d['likert_question'].map(self.emp_engagement_sections_dict),
                        engagement_level=lambda d: d['answer_int'].apply(self._engagement_scores_gallup))
                .assign(engagement_score=lambda d: d['engagement_level'].map({'Actively disengaged': -1,
                                                                              'Not engaged': 0, 'Engaged': 1}),
                        engaged=lambda d: (d['answer_int'] == 5).astype(int),
                        not_engaged=lambda d: d['answer_int'].apply(lambda x: int(2 < x < 5)),
                        actively_disengaged=lambda d: (d['answer_int'] <= 2).astype(int)
                        ))

    def step_6_last_column_work(self, step_5_df):
        """
        Fill in team, company, date_added, name_optional, survey_date
        Args:
            step_5_df:

        Returns:

        """
        return (step_5_df
                    .copy()  # noqa
                    .assign(team=lambda d: None if 'team' not in d.columns else d['team'],
                            name_optional=lambda d: None if 'name_optional' not in d.columns else d['name_optional'],
                            company=self.company,
                            role=lambda d: None if 'role' not in d.columns else d['role'],
                            date_added=datetime.datetime.now().date(),
                            survey_date=pd.to_datetime(self.survey_date).to_pydatetime(),
                            supervisor=lambda d: None if 'supervisor' not in d.columns else d['supervisor'],
                            location=lambda d: None if 'location' not in d.columns else d['location'],
                            anything_else_share=lambda d: None if 'anything_else_share' not in d.columns else d['anything_else_share']
                           )
                    .loc[:, self.final_columns_order])

    @staticmethod
    def step_7_validate_output(step_6_df):
        ge_df = ge.from_pandas(pandas_df=step_6_df)
        validation_results = ge_df.validate(expectations_config=EXPECTATIONS_PATH.as_posix(), only_return_failures=True)

        success = validation_results.get('success')
        if not success:
            print(step_6_df.sample(10))
            raise ValueError(f"Outgoing expectations failed: \n{validation_results}")

        return success

    @staticmethod
    def step_8_report(step_6_df):
        """
        Make a report of the engagement stats
        Returns:

        """
        from rich.console import Console
        from rich.table import Table

        table = Table(title="Survey stats")
        table.add_column("Metric", style="cyan")
        table.add_column("Value", justify="right", style="magenta")
        table.add_column("Description", style="green")

        def fmt(x):
            return str(round(x, 3))
        
        data_out = []

        d = step_6_df.copy()
        n = d.shape[0]

        promoters = d.query("NPS_group == 1").shape[0]
        detractors = d.query("NPS_group == -1").shape[0]
        enps = ((promoters / n) - (detractors / n)) * 100
        table.add_row("eNPS Score", fmt(enps), "eNPS score")
        table.add_row("Average LtR rating", fmt(d['nps_score'].mean()), "Avg of likelihood to rec. scores")
        table.add_row("% Promoters", fmt(promoters / n), "Rated likelihood to recommend 9-10")
        table.add_row("% Neutral", fmt(d.query("NPS_group == 0").shape[0] / n), "Rated likelihood to recommend 7-8")
        table.add_row("% Detractors", fmt(detractors / n), "Rated likelihood to recommend < 7")
        table.add_row("Total", str(
            (d.query("NPS_group == 1").shape[0] +  # noqa
             d.query("NPS_group == 0").shape[0] + d.query("NPS_group == -1").shape[0]) / n
        ), "Should be 1")

        # Save our info
        data_out.extend([
            {"Metric": "eNPS Score", "Value": fmt(enps), "Description": "eNPS score"},
            {"Metric": "Average LtR rating", "Value": fmt(d['nps_score'].mean()), "Description": "Avg of likelihood to rec. scores"},
            {"Metric": "% Promoters", "Value": fmt(promoters / n), "Description": "Rated likelihood to recommend 9-10"},
            {"Metric": "% Neutral", "Value": fmt(d.query("NPS_group == 0").shape[0] / n), "Description": "Rated likelihood to recommend 7-8"},
            {"Metric": "% Detractors", "Value": fmt(detractors / n), "Description": "Rated likelihood to recommend < 7"},
            {"Metric": "Total", 
             "Value": str((d.query("NPS_group == 1").shape[0] + d.query("NPS_group == 0").shape[0] + d.query("NPS_group == -1").shape[0]) / n),
             "Description": "Should be 1"},
            {"Metric": '---', "Value": '---', "Description": '---'},
        ])

        # Filter to valid answers for engagement stats
        d = d.query("answer_int < 6")
        n = d.shape[0]
        table.add_row("---", "---", "---", )
        table.add_row("Average Eng. Score", fmt(d['answer_int'].mean()), "Avg of Likert questions given.")
        table.add_row("% Engaged", fmt(d['engaged'].sum() / n), "% of those who rated a 5")
        table.add_row("% Not engaged", fmt(d['not_engaged'].sum() / n), "% of those who rated a 3-4")
        table.add_row("% Disengaged", fmt(d['actively_disengaged'].sum() / n), "% of those who rated a 1-2")
        table.add_row("Total",
                      str((d['engaged'].sum() + d['not_engaged'].sum() + d['actively_disengaged'].sum()) / n),
                      "Should be 1")

        data_out.extend([
            {"Metric": "Average Eng. Score", "Value": fmt(d['answer_int'].mean()), "Description": "Avg of Likert questions given."},
            {"Metric": "% Engaged", "Value": fmt(d['engaged'].sum() / n), "Description": "% of those who rated a 5"},
            {"Metric": "% Not engaged", "Value": fmt(d['not_engaged'].sum() / n), "Description": "% of those who rated a 3-4"},
            {"Metric": "% Disengaged", "Value": fmt(d['actively_disengaged'].sum() / n), "Description": "% of those who rated a 1-2"},
            {"Metric": "Total", "Value": str((d['engaged'].sum() + d['not_engaged'].sum() + d['actively_disengaged'].sum()) / n), "Description": "Should be 1"},
        ])

        console = Console(record=True)
        console.print(table)

        return  data_out

    def main(self):
        # Step 1 is now done at class init
        s1_df = self.step_2a_remove_incomplete_entries()
        s2_df, step_2_rename_dict = self.step_2_establish_columns(s1_df)
        s3_df = self.step_3_melt(s2_df, step_2_rename_dict)
        s4_df = self.step_4_map_answer_columns(s3_df)
        s5_df = self.step_5_other_col_calc(s4_df)
        s6_df = self.step_6_last_column_work(s5_df)
        self.step_7_validate_output(s6_df)
        r = self.step_8_report(s6_df)
        return r, s6_df
