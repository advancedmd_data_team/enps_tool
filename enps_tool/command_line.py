#!/usr/bin/env python3

import os
import re
import datetime
import pathlib

import pandas as pd
import PySimpleGUI as sg

from enps_tool.core import Survey

sg.set_options(font=('Times New Roman', 14))

# For the first thing, we want them to select the survey file.
DATA_FILE = sg.popup_get_file('Survey Data File',
                              file_types=(("CSV Files", "*.csv"), ("Excel Files", "*.xls*"),), 
                              initial_folder=os.path.dirname(__file__),
                              history=True)
if DATA_FILE is None:
    exit()

TODAY = datetime.datetime.today().date()


def name(name, size=15, tooltip=None):
    dots = size - len(name) - 2
    return sg.Text(name + ' ' + ' ' * dots, size=(size, 1), tooltip=tooltip, justification='l', pad=(10, 0))


def read_in_data_file(data_file):
    """Read in the raw survey output file, whether CSV or Excel. Return the Table read into a display.
    
    Args:
        data_file (str | os.path like): Path to survey file
    """
    print(data_file)
    if data_file.endswith('.csv'):
        df = pd.read_csv(data_file)
    elif data_file.endswith('.xlsx'):
        df = pd.read_excel(data_file)
    else:
        sg.popup_error("Not sure how to read in this kind of file. Expected a CSV or Excel file.")

    right_click_menu = ['&Right', ['Copy']]

    # Format the data so it works with the GUI
    headings = df.columns.tolist()
    values = df.values.tolist()
    data_layout = [
        [
        sg.Table(
            values=values,
            headings=headings,
            auto_size_columns=False,
            max_col_width=10,
            col_widths=list(map(lambda x: len(x) + 1, headings)),
            # display_row_numbers=True,
            select_mode='extended',
            alternating_row_color='gray',
            enable_events=True, 
            justification='left',
            # enable_click_events=True, 
            right_click_menu=right_click_menu,
            vertical_scroll_only=False,
            key='_table_',
        )
        ]
    ]
    return data_layout, values, headings


def _generate_default_choices(header_values, phrase_pat):
    """
    This function uses key words in each of the engagement questions to pre-populate the engagements.
    If multiple are found, it returns only the first in the list, else None.
    
    Args:
        header_values (list): A list of the columns to run through
        phrase_pat (str): regex search pattern
    """

    col = [i for i in header_values if re.search(phrase_pat, i, re.I)]
    if col:
        return col[0]


def pop_up_report(title, dataf, data_raw):
    window = sg.Window(title, [
        [ sg.Table(values=dataf.values.tolist(),
                   headings=dataf.columns.tolist(), 
                   auto_size_columns=True,
                   expand_x=True, expand_y=True)],
         [sg.Exit(),sg.Button("Save data out to CSV file?", enable_events=True, bind_return_key=True, )],
         [sg.Text(key='_save_message_')]
    ],
        finalize=True,
        keep_on_top=True,
        grab_anywhere=True,
        resizable=True,
        auto_size_buttons=True,
    )
    while True:
        event, values = window.read()
        if event in (sg.WINDOW_CLOSED, "Exit"):
            break
        if event == "Save data out to CSV file?":
            save_out_loc = pathlib.Path.home().joinpath(f"processed_enps_survey_data_{TODAY}.csv")
            data_raw.to_csv(save_out_loc, index=False)
            # exit()
            msg = window['_save_message_']
            msg.update(f"Saved the file to: {save_out_loc}")

    window.close()
    

def main():
    """Creates the main layout, reads in the data file, accepts args, processes the file."""

    dat_layout, data, headers = read_in_data_file(DATA_FILE)

    general_survey_info_layout = [
        [
            sg.Text('Survey Start Date', size=(14,1)),
            sg.InputText(key='_date_', default_text=TODAY),
            sg.CalendarButton("Select Date", close_when_date_chosen=True, target="_date_", format='%Y-%m-%d', size=(10,1))
        ],
        [sg.Text("Company:"), sg.InputText(default_text="CES", key='_company_')],
    ]

    surv_content_layout = [
        [sg.Text("These are the 12 core questions we use for measuring eNPS and engagement, as worded in your survey:")],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'expected'), enable_events=True, readonly=True, key='-LIK1-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'materials|equipment'), enable_events=True, readonly=True, key='-LIK2-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'opportunity'), enable_events=True, readonly=True, key='-LIK3-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'praise'), enable_events=True, readonly=True, key='-LIK4-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'care'), enable_events=True, readonly=True, key='-LIK5-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'encourag'), enable_events=True, readonly=True, key='-LIK6-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'opinion'), enable_events=True, readonly=True, key='-LIK7-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'mission'), enable_events=True, readonly=True, key='-LIK8-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'fellow employees|coworkers'), enable_events=True, readonly=True, key='-LIK9-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'best friend'), enable_events=True, readonly=True, key='-LIK10-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'progress'), enable_events=True, readonly=True, key='-LIK11-', expand_x=False)],
        [sg.Combo(headers, default_value=_generate_default_choices(headers, 'learn and grow'), enable_events=True, readonly=True, key='-LIK12-', expand_x=False)],
    ]

    text_analysis_layout = [
        [sg.Text("These are the questions used in analyzing the free-text answers. Please enter them in as they appear in the Spreadsheet (be mindful of any whitespaces).")],
        [name("Reason for nps score text field:", size=25), sg.Combo(headers, default_value=_generate_default_choices(headers, 'reason for|primary'), enable_events=True, readonly=True, key='nps_reason_text_col')],
        [name("Anything else text field:", size=25), sg.Combo(headers, default_value=_generate_default_choices(headers, 'like to share'), enable_events=True, readonly=True, key='anything_else_text_col')],
    ]

    general_columns_layout = [
        [sg.Text("These are the questions used to segment analysis. Please select the applicable columns.")],
        [sg.Text("The `ID` field may be generated automatically by the survey software, or manually. "
                "In either case, each survey taker should have one unique number.")],
        [name("ID field:", size=15),         sg.Combo([None] + headers, default_value=_generate_default_choices(headers, 'index|ID'), enable_events=True, readonly=True, key='id_col')],
        [name("NPS score field:"),  sg.Combo([None] + headers, default_value=_generate_default_choices(headers, 'to recommend'), enable_events=True, readonly=True, key='nps_score_col')],
        [name("Department field:"), sg.Combo([None] + headers, default_value=_generate_default_choices(headers, 'dept|Department'), enable_events=True, readonly=True, key='dept_col')],
        [name("Role field:"),       sg.Combo([None] + headers, default_value=_generate_default_choices(headers, 'role'), enable_events=True, readonly=True, key='role_col')],
        [name("Team field:"),       sg.Combo([None] + headers, default_value=_generate_default_choices(headers, 'team'), enable_events=True, readonly=True, key='team_col')],
        [name("Supervisor field:"), sg.Combo([None] + headers, default_value=_generate_default_choices(headers, 'leader|your supervisor'), enable_events=True, readonly=True, key='supervisor_col')],
        [name("Name field:"),       sg.Combo([None] + headers, default_value=_generate_default_choices(headers, 'optional'), enable_events=True, readonly=True, key='name_optional_col')],
    ]

    main_layout = [
        [sg.Menu([['File', ['Exit']], ['Edit', ['Edit Me', ]]],  k='-MENU-', p=0)],
        [sg.Text("Data Preview: ")],
        dat_layout,
        [sg.TabGroup([[ 
            sg.Tab("General Info", general_survey_info_layout),
            sg.Tab("Engagement Questions", surv_content_layout),
            sg.Tab("General Columns", general_columns_layout),
            sg.Tab("Text Columns", text_analysis_layout),
        ]], key='-TAB GROUP-', expand_x=True, expand_y=True)],
        # End layout
        [sg.Exit(button_color='red'), sg.Button("Process", button_color='green', bind_return_key=True)],
    ]

    window = sg.Window(
        "eNPS processor",
        main_layout,
        finalize=True,
        grab_anywhere=True,
        resizable=True,
        auto_size_buttons=True,
    )

    while True:
        event, values = window.read()
        # Place our data file in our values
        # print(event, values)

        if event in (sg.WINDOW_CLOSED, "Exit"):
            break

        elif event == 'Process':
            other_columns = {
                'id': values.get('id_col'),
                'dept': values.get('dept_col'),
                'role': values.get('role_col'),
                'team': values.get('team_col'),
                'supervisor': values.get('supervisor_col'),
                'name_optional': values.get('name_optional_col'),
                'nps_score': values.get('nps_score_col'),
                'nps_text_answer': values.get('nps_reason_text_col'),
                'anything_else_share': values.get('anything_else_text_col'),
            }
            surv = Survey(
                data_file=DATA_FILE,
                likert_columns=[v for k, v in values.items() if k.startswith('-LIK')],
                other_columns=other_columns,
                survey_date=values.get('_date_'),
                company=values.get('_company_'),
                remove_incomplete=values.get('_rm_incompletes_'),
                complete_indicator_col=values.get('_complete_col_')
            )
            # Analyze the data
            report_data, processed_survey_dataf = surv.main()

            df = pd.DataFrame(report_data)
            pop_up_report(title="The results are in!", dataf=df, data_raw=processed_survey_dataf)

    window.close()


if __name__ == '__main__':
    main()
