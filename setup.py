from setuptools import setup, find_packages

setup(
    name='enps_tool',
    author='Nathan Cheever',
    version='0.1.0',
    packages=find_packages(exclude=['tests', '.idea', '.vs_code', '.cache', '__pycache__']),
    install_requires=[
        'PySimpleGUI==4.60.5',         # For the GUI
        'numpy==1.22.4',               # For working with dataframes
        'pandas==1.4.0',               # For working with dataframes
        'openpyxl==3.1.2',             # To open Excel files
        'great-expectations==0.4.3',   # Runs final data tests to ensure the script/data are working correctly
        'SQLAlchemy==2.0.9',
        'attrs==22.2.0',
        'jsonschema==4.17.3',
        'pyrsistent==0.19.3',
        'python-dateutil==2.8.2',
        'pytz==2022.7.1',
        'scipy==1.7.3',
        'six==1.12.0',
        'typing_extensions==4.7.1',
        'rich',                        # For the console output
    ],
    include_package_data=True,
    entry_points = {
        'console_scripts': ['enps_tool=enps_tool.command_line:main'],
    }
)
