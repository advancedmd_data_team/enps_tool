## enps_tool
This is a command-line utility, written in Python, used for calculating the employee net promoter score (eNPS) and engagement scores for an employee Net Promoter survey.


### Installation
The below command will install the core Python files used. It will also install the needed Python libraries needed. The main ones are Pandas, PySimpleGUI, and great-expectations.

This assumes Python 3, preferrably 3.8 or higher, is installed on the machine you're using.

#### Mac
```bash
# (Command line terminal)
python -m pip install -U git+https://gitlab.com/advancedmd_data_team/enps_tool.git
```
That will install the app and the necessary dependencies.

To start it up, run:
```bash
# (Command line terminal)
> enps_tool
```

#### Windows

 - Download the code from the [repo](https://gitlab.com/advancedmd_data_team/enps_tool) as a zip, and unzip it.
 - Install dependencies with `python -m pip install .`
 - Run the code with `python enps_tool/command_line.py`

It might take a second to start up.

### Usage

That will startup the tool. Browse to the file with the raw survey data (usually downloaded from Google Forms/Microsoft Forms). It should be either an Excel or CSV file format.
![Startup Screen](images/startup-screen.png)
Once it's on, go through and select the columns that align. The app will try to find the right columns automatically, but be sure to inspect them to make sure they're right.

![Main screen](images/main-screen.png)
Once all the fields are selected, click `Process`. That will run the analysis and format the data in a way that's ready for the dashboard style.

![Results screen](images/results-screen.png)

You can also choose to download this data as a CSV file, so you can import it into a database of your choosing.
